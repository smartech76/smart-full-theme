<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SmartTeach
 */

get_header();
?>
    <div class="container-sliders">
        <div id="container-sliders-parallax">
            <div id="main-slider">
                <div class="main-slider-item">
                    <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                    <div id="slide_one" class="slider-title hidden-xs title-block">
                        <img src="<?php echo get_template_directory_uri() .'/images/smart-logo.png'?>" alt="">
                        <h1>Тестовый заголовок</h1>
                        <p>Тестовое описание</p>
                        <button class="btn-red btn-rectangle">Заказать</button>
                    </div>
                </div>
                <div class="main-slider-item">
                    <img src="<?php echo get_template_directory_uri() . '/images/1.jpg' ?>">
                    <div id="slide_two" class="slider-title hidden-xs title-block">
                        <img src="<?php echo get_template_directory_uri() .'/images/smart-logo.png'?>" alt="">
                        <h1>Тестовый заголовок</h1>
                        <p>Тестовое описание</p>
                        <button class="btn-red btn-rectangle">Заказать</button>
                    </div>
                </div>
                <div class="main-slider-item">
                    <img src="<?php echo get_template_directory_uri() . '/images/3.jpg' ?>">
                    <div id="slide_three" class="slider-title hidden-xs title-block">
                        <img src="<?php echo get_template_directory_uri() .'/images/smart-logo.png'?>" alt="">
                        <h1>Тестовый заголовок</h1>
                        <p>Тестовое описание</p>
                        <button class="btn-red btn-rectangle">Заказать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-content content-background-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Занимаемся разработкой сайтов более 3х лет, мы можем реализовать
                        задуманное на бумаге, в то что будет приносить прибыль в жизни. Готовы выполнить сложные проекты в
                        разумные сроки и суммы. Поможем вашим клиентм найти путь к вашей компании в интернете.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-content content-background-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Лендинг</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Одностраничный сайт для быстрых продаж и промоакций. Отличное решение при старте бизнеса. Обеспечит поток клиентов и продвижение бренда в сети. Быстрая реализация.</p>
                </div>
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Сайт визитка</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Имидж компании – это ее сайт. Современный дизайн и удобство для пользователей. Сайт визитка производит впечатление и привлекает партнеров к сотрудничеству. Кроме того, можно размещать фотогалереи, описание деятельности и многое другое.</p>
                </div>
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Интернет магазин</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Полнофункциональный магазин с уникальным дизайном, возможностью регистрации и онлайн оплаты. Создадим уникальное описание товаров, продвинем предложение на Яндекс Маркет.</p>
                </div>
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Корпоративный сайт</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Представительский сайт для крупных организаций. Уникальные решения для разных сфер деятельности. Недвижимость, медицина, юридические услуги, торговля, производство.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-tiles">
        <div class="container-fluid">
            <div class="row row-tile">
                <div class="col-sm-4 col-md-4 content-tile content-background-gray">
                    <h2 class="content-header-middle">Разработка сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Создаем уникальный дизайн, мобильную верстку, интеграцию с 1С, наполнение уникальным контентом. Используем современные технологии и проверенные временем решения.</p>
                </div>
                <div class="col-sm-4 col-md-4 content-tile-image content-background-image back-image-one">
                </div>
                <div class="col-sm-4 col-md-4 content-tile content-background-green">
                    <h2 class="content-header-middle">Сопровождение</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Нужно поднять сайт в десятку лидеров, развить группу в ВК и Инстаграм, сделать сайт продающим, собрать статистику и настроить Яндекс Директ? Отдел сопровождения SmartTech всегда готов пойти в бой за успеx!</p>
                </div>
            </div>
            <div class="row row-tile">
                <div class="col-sm-4 col-md-4 content-tile-image content-background-image back-image-two">
                </div>
                <div class="col-sm-4 col-md-4 content-tile content-background-red">
                    <h2 class="content-header-middle">Техническое обслуживание</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Вам сделали сайт, и он работает с багами? Остались неровности в верстке? У вас есть идеи, и вы
                        не знаете к кому обратиться? Наш технический отдел поправит все ошибки на вашем сайте, добавит резервное копирование и систему контроля версий. Ваш сайт будет защищен от вирусов и потери данных.</p>
                </div>
                <div class="col-sm-4 col-md-4 content-tile-image content-background-image back-image-three">
                </div>
            </div>
        </div>
    </div>

    <div class="container-wood container-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
                <div class="col-sm-3 col-md-3">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
            </div>
        </div>
    </div>

    <div id="request-call" class="container-request-call">
        <div id="container-request-call-parallax" class="hidden-xs">
        </div>
        <div class="block-request-call container-content" id="container-parallax-xs">
            <h2 class="content-header-middle text-white">Создание сайтов</h2>
            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачной попыткой решения огромного
                спектра проблем при построении лейаутов в css.</p>
            <button class="btn-red btn-rectangle">Заказать консультацию</button>
        </div>
    </div>

    <div class="container-content content-background-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
                <div class="col-sm-4 col-md-4">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
                <div class="col-sm-4 col-md-4">
                    <h2 class="content-header-middle">Создание сайтов</h2>
                    <div class="container-hexagon">
                    </div>
                    <p class="content-paragraph-middle">Flexbox по праву можно назвать удачной попыткой решения огромного
                        спектра проблем при построении лейаутов в css. Но прежде чем перейти к его описанию, давайте
                        выясним, что же не так со способами верстки, которыми мы пользуемся сейчас?</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-tiles">
        <div class="container-fluid">
            <div class="row">
                <div class="container-image-tile">
                    <div class="block-image-tile">
                        <div class="block-image-tile-img">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="block-image-tile-info">
                            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачным
                                решением</p>
                        </div>
                    </div>
                    <div class="block-image-tile">
                        <div class="block-image-tile-img">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="block-image-tile-info">
                            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачным
                                решением</p>
                        </div>
                    </div>
                    <div class="block-image-tile">
                        <div class="block-image-tile-img">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="block-image-tile-info">
                            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачным
                                решением</p>
                        </div>
                    </div>
                    <div class="block-image-tile">
                        <div class="block-image-tile-img">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="block-image-tile-info">
                            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачным
                                решением</p>
                        </div>
                    </div>
                    <div class="block-image-tile">
                        <div class="block-image-tile-img">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="block-image-tile-info">
                            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачным
                                решением</p>
                        </div>
                    </div>
                    <div class="block-image-tile">
                        <div class="block-image-tile-img">
                            <img src="<?php echo get_template_directory_uri() . '/images/5.jpg' ?>">
                        </div>
                        <div class="block-image-tile-info">
                            <p class="content-paragraph-middle text-white">Flexbox по праву можно назвать удачным
                                решением</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        <?php if (have_posts()): while (have_posts()): the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; endif; ?>
    </section>
<?php
get_footer();

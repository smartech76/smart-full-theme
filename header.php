<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SmartTeach
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>

    <div class="container-desktop-header">
        <div class="container-fluid">
            <div class="row">
                <div class="hidden-md hidden-lg col-xs-4 col-sm-3">
                    <div id="header-tablet-menu" class="header-icon pull-left">
                        <div id="header-tablet-menu-icon" class="">
                            <span class="glyphicon glyphicon-th-large"></span>
                        </div>
                        <div id="header-tablet-close-icon" class="hide">
                            <span class="glyphicon glyphicon-remove"></span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-6 col-md-3">
                    <div class="header-logo">
                        <img src="<? echo get_template_directory_uri() . '/images/smart-logo.png'?>" alt="text" class="header-logo-desktop">
                        <img src="<? echo get_template_directory_uri() . '/images/smart-logo.png'?>" alt="text" class="header-logo-mobile">
                    </div>
                </div>
                <div class="hidden-xs hidden-sm col-md-6">
                    <div class="header-main-menu">
                        <nav class="main-navigation">
                            <? wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'top-menu')); ?>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-3 col-md-3">
                    <div class="header-icon header-search pull-right">
                        <div id="header-search-icon" class="header-search-icon">
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                        <div id="header-close-icon" class="header-search-icon hide">
                            <span class="glyphicon glyphicon-remove"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!--header tablet menu-->
            <div id="header-menu-widget" class="header-menu-widget-container">
                <div class="header-menu-widget">
                    <div class="header-tablet-menu-list">
                        <nav class="main-navigation">
                            <? wp_nav_menu(array('menu' => 'top-menu', 'menu_class' => 'top-menu')); ?>
                        </nav>
                    </div>
                </div>
            </div>
            <!--header search widget-->
            <div id="header-search-widget" class="header-search-widget-container">
                <div class="header-search-widget">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>

</header>

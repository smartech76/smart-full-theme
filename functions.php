<?php
/**
 * SmartTeach functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SmartTeach
 */

if ( ! function_exists( 'smartteach_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function smartteach_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on SmartTeach, use a find and replace
		 * to change 'smartteach' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'smartteach', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'smartteach' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'smartteach_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'smartteach_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function smartteach_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'smartteach_content_width', 640 );
}
add_action( 'after_setup_theme', 'smartteach_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function smartteach_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'smartteach' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'smartteach' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'smartteach_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function smartteach_scripts() {
	wp_enqueue_style( 'smartteach-style', get_stylesheet_uri() );

	wp_enqueue_script( 'smartteach-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'smartteach-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

    wp_register_script('jquery-3.3.1', get_template_directory_uri() . '/js/jquery-3.3.1/jquery-3.3.1.min.js');
    wp_register_script('slick', get_template_directory_uri() . '/js/slick/slick.min.js');
    wp_register_script('parallax', get_template_directory_uri() . '/js/parallax.js');
    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js');
    wp_register_script('sliders', get_template_directory_uri() . '/js/sliders.js');
    wp_register_script('widgets', get_template_directory_uri() . '/js/widgets.js');
    wp_register_script('image-tiles', get_template_directory_uri() . '/js/image-tiles.js');


    wp_enqueue_script('jquery-3.3.1');
    wp_enqueue_script('slick');
    wp_enqueue_script('parallax');
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('sliders');
    wp_enqueue_script('widgets');
    wp_enqueue_script('image-tiles');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'smartteach_scripts' );

function enqueue_styles()
{
    wp_register_style('slick', get_template_directory_uri() . '/css/slick.css');
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css');
    wp_register_style('header', get_template_directory_uri() . '/css/header.css');
    wp_register_style('main', get_template_directory_uri() . '/css/main.css');
    wp_register_style('sliders', get_template_directory_uri() . '/css/sliders.css');
    wp_register_style('content-blocks', get_template_directory_uri() . '/css/content-blocks.css');
    wp_register_style('custom-checkbox', get_template_directory_uri() . '/css/custom-checkbox.css');

    wp_enqueue_style('robot-regular', get_stylesheet_uri());


    wp_enqueue_style('bootstrap');
    wp_enqueue_style('slick');
    wp_enqueue_style('header');
    wp_enqueue_style('main');
    wp_enqueue_style('sliders');
    wp_enqueue_style('content-blocks');
    wp_enqueue_style('custom-checkbox');
    wp_enqueue_style('robot-regular');

}

add_action('wp_enqueue_scripts', 'enqueue_styles');


if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


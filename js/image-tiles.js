$(document).ready(function () {
    
    var tileWidth;
    var $containerTiles = $(".container-image-tile");
    
    function setTileHeight() {
        tileWidth = $(".block-image-tile").innerWidth();
        $(".block-image-tile").height(tileWidth);
    }
    
    //When resizing the window, resize the tiles
    $(window).resize(function () {
        setTileHeight();
    });
    
});
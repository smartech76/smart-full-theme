$(document).ready(function () {

    //Tiles
    function initTiles() {

        function resizeTiles() {
            $(".row-tile").each(function (indx, element) {
                var rowHeight = $(element).children(".content-tile").innerHeight();
                $(element).children(".content-tile-image").height(rowHeight);
            });
        }

        $(window).resize(function () {
            resizeTiles();
        });

        resizeTiles();
    }

    //header search widget
    function initSearchWidget() {

        var isOpen = false;
        var $buttonSearch = $(".header-search");
        var $widgetSearch = $("#header-search-widget");
        var $searchIcon = $("#header-search-icon");
        var $closeIcon = $("#header-close-icon");

        function closeWidgetSearch() {
            $widgetSearch.css('right', -475);
            $searchIcon.removeClass("hide");
            $closeIcon.addClass("hide");
            isOpen = false;
        }

        function openWidgetSearch() {
            $widgetSearch.css('right', 0);
            $searchIcon.addClass("hide");
            $closeIcon.removeClass("hide");
            isOpen = true;
        }

        $buttonSearch.click(function () {
            if (isOpen) {
                closeWidgetSearch();
            } else {
                openWidgetSearch();
            }
        });

        //Close widget when clicking outside the area
        $(document).mouseup(function (e) {
            if (!$widgetSearch.is(e.target)
                && $widgetSearch.has(e.target).length === 0
                && isOpen
                && !$buttonSearch.is(e.target)
                && $buttonSearch.has(e.target).length === 0) {
                closeWidgetSearch();
            }
        });
    }

    //header tablet menu
    function initTabletMenuWidget() {

        var isOpen = false;
        var $buttonTabletMenu = $("#header-tablet-menu");
        var $widgetTabletMenu = $("#header-menu-widget");
        var $tabletMenuIcon = $("#header-tablet-menu-icon");
        var $tabletCloseIcon = $("#header-tablet-close-icon");

        function closeWidgetTabletMenu() {
            $widgetTabletMenu.css('left', -485);
            $tabletMenuIcon.removeClass("hide");
            $tabletCloseIcon.addClass("hide");
            isOpen = false;
        }

        function openWidgetTabletMenu() {
            $widgetTabletMenu.css('left', 0);
            $tabletMenuIcon.addClass("hide");
            $tabletCloseIcon.removeClass("hide");
            isOpen = true;
        }

        $buttonTabletMenu.click(function () {
            if (isOpen) {
                closeWidgetTabletMenu();
            } else {
                openWidgetTabletMenu();
            }
        });

        //Close widget when clicking outside the area
        $(document).mouseup(function (e) {
            if (!$widgetTabletMenu.is(e.target)
                && $widgetTabletMenu.has(e.target).length === 0
                && isOpen
                && !$buttonTabletMenu.is(e.target)
                && $buttonTabletMenu.has(e.target).length === 0) {
                closeWidgetTabletMenu();
            }
        });
    }

    //header background
    function initHeaderBackground() {

        var $header = $(".container-desktop-header");
        var $widgetSearch = $(".header-search-widget");
        var $widgetSearchInput = $(".header-search-widget-input");
        var $widgetTabletMenu = $(".header-menu-widget");
        var headerHeight = $header.innerHeight();
        var widgetSearchHeight = $widgetSearch.innerHeight();
        var clientHeight = document.documentElement.clientHeight;
        var scrollTop;

        $(window).bind('scroll', function (e) {

            scrollTop = window.pageYOffset || document.documentElement.scrollTop;

            if (scrollTop > clientHeight - headerHeight - widgetSearchHeight) {
                $header.addClass("header-gray");
                $widgetSearch.addClass("header-gray");
                $widgetTabletMenu.addClass("header-gray");
                $widgetSearchInput.removeClass("dark-input");
            } else {
                $header.removeClass("header-gray");
                $widgetSearch.removeClass("header-gray");
                $widgetTabletMenu.removeClass("header-gray");
                $widgetSearchInput.addClass("dark-input");
            }
        });
    }
    //Tiles
    initTiles();
    //header search widget
    initSearchWidget();
    //header tablet menu
    initTabletMenuWidget();
    //header background
    initHeaderBackground();

});
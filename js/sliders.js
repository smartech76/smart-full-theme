$(document).ready(function () {

    var clientHeight = document.documentElement.clientHeight;
    
    $(".container-sliders").css("height", clientHeight);
    $("#container-sliders-parallax").css("height", clientHeight);
    
    $("#main-slider").slick({
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>",
    });
    
    $(".slick-arrow").css("top", clientHeight / 2.3);

});